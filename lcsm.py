#Import Data -> Dictionary;
#Find (k, v) where k has shortest len(v) in dict;
#take all possible subsubstrings of said v;
#iterate over other (k, v) where if v doesnt contain a subsubstring, eliminate substring;
#once iterated, output longest v;

def toDic(st):
    if len(st) < 1:
        return
    name = st[:dataStart(st)]
    if st.find(">",1) == -1:
        data = st[dataStart(st):]
        d[name] = data
        return
    else:
        data = st[dataStart(st):st.find(">", 1)]
        d[name] = data
    toDic(st[len(name)+len(data):])

def dataStart(st):
    for i in range(10, len(st)):
        if not st[i].isdigit():
            return i
        
def getLowest(dic):
    low = float('inf')
    id = ""
    for k, v in dic.iteritems():
        if len(v) < low:
            low = len(v)
            id = k
    return id

def getHighest(dic, n):
    high = 0
    id = ""
    for k, v in dic.iteritems():
        if not v == n:
            continue
        if len(k) > high:
            high = len(k)
            id = k
    return id

def genSubstrings(st):
    for s in range(0,len(st)):
        for e in range(s,len(st)):
                subs[st[s:e]] = 0
    subs[st] = 0
    
file = open('D:/Downloads/rosalind_lcsm.txt')
f = ""
for line in file:
        f += line.rstrip()
file.close()

d = {}
subs = {}

toDic(f)
genSubstrings(d[getLowest(d)])
del subs[''] # remove error possibility

for v in d.itervalues():
    for k in subs.iterkeys():
        if k in v:
            subs[k] += 1        

print getHighest(subs, len(d))
