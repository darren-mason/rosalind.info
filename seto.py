# sets
u = 10
a = [1, 2, 3, 4, 5]
b = [2, 8, 5, 10]
aUb = [] # a union b
aIb = [] # a intersect b
aDb = [] # A-B
bDa = [] # B-A
AC = [] # U - A
BC = [] # U - B

for e in range(1, u + 1):
    if e in a or e in b: # union
        aUb.append(e) 
    if e in a and e in b: # intersect
        aIb.append(e)
    if e in a and e not in b: # a, not b
        aDb.append(e)
    if e not in a and e in b: # b, not a
        bDa.append(e)
    if e not in a: # set, not a
        AC.append(e)
    if e not in b: # set, not b
        BC.append(e)

print aUb
print aIb
print aDb
print bDa
print AC
print BC
