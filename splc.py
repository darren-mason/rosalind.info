# Create dictionary
d = {
    'UUU': 'F', 'CUU': 'L', 'AUU': 'I', 'GUU': 'V', 'UUC': 'F', 'CUC': 'L',
    'AUC': 'I', 'GUC': 'V', 'UUA': 'L', 'CUA': 'L', 'AUA': 'I', 'GUA': 'V',
    'UUG': 'L', 'CUG': 'L', 'AUG': 'M', 'GUG': 'V', 'UCU': 'S', 'CCU': 'P',
    'ACU': 'T', 'GCU': 'A', 'UCC': 'S', 'CCC': 'P', 'ACC': 'T', 'GCC': 'A',
    'UCA': 'S', 'CCA': 'P', 'ACA': 'T', 'GCA': 'A', 'UCG': 'S', 'CCG': 'P',
    'ACG': 'T', 'GCG': 'A', 'UAU': 'Y', 'CAU': 'H', 'AAU': 'N', 'GAU': 'D',
    'UAC': 'Y', 'CAC': 'H', 'AAC': 'N', 'GAC': 'D', 'UAA': 'Stop',
    'CAA': 'Q', 'AAA': 'K', 'GAA': 'E', 'UAG': 'Stop', 'CAG': 'Q',
    'AAG': 'K', 'GAG': 'E', 'UGU': 'C', 'CGU': 'R', 'AGU': 'S', 'GGU': 'G',
    'UGC': 'C', 'CGC': 'R', 'AGC': 'S', 'GGC': 'G', 'UGA': 'Stop',
    'CGA': 'R', 'AGA': 'R', 'GGA': 'G', 'UGG': 'W', 'CGG': 'R', 'AGG': 'R',
    'GGG': 'G'
    }

dx = {}

file = open('D:/Downloads/rosalind_splc.txt')
f = ""
for lines in file:
    f += lines.rstrip()
file.close()


def toDic(st):
    if len(st) < 1:
        return
    name = st[:dataStart(st)]
    if st.find(">",1) == -1:
        data = st[dataStart(st):]
        dx[name] = data
        return
    else:
        data = st[dataStart(st):st.find(">", 1)]
        dx[name] = data
    toDic(st[len(name)+len(data):])

def dataStart(st):
    for i in range(10, len(st)):
        if not st[i].isdigit():
            return i

def getLast():
    (k, v) = d.popitem()
    return v

def toRNA(v):
    v = v.replace('T', 'U')
    n = 0
    m = 1
    o = ""
    while m*3 != len(v): # test condition for end of input
        x = d[v[n*3:m*3]] # get RNA key for 3-letter mRNA
        if x == 'Stop': # test condition for stopping
            break
        o += x # concatenate output string
        n += 1 ##
        m += 1 # iterate output
    return o

toDic(f)
val = dx.pop('>Rosalind_3446')

for v in dx.values():
    val = val.replace(v,"")
    
print toRNA(val)








    
