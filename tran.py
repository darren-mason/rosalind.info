from decimal import *
getcontext().prec = 11

def toStrings(st):
    s = getData(st)
    st = st.replace(st[:14],"")
    st = st.replace(s,"")
    t = st[dataStart(st):]
    return (s, t)

def getData(st):
    return st[dataStart(st):st.find(">",1)]
    

def dataStart(st):
    for i in range(10, len(st)):
        if not st[i].isdigit():
            return i

file = open('D:/Downloads/rosalind_tran.txt')
data = ""
for lines in file:
    data += lines.rstrip()
file.close()

s1, s2 = toStrings(data)
transitions, transversions = 0, 0

for i in range(0, len(s1)):
    if s1[i] == 'C' and s2[i] == 'T':
        transitions += 1
        continue
    if s1[i] == 'T' and s2[i] == 'C':
        transitions += 1
        continue
    if s1[i] == 'A' and s2[i] == 'G':
        transitions += 1
        continue
    if s1[i] == 'G' and s2[i] == 'A':
        transitions += 1
        continue
    if s1[i] == s2[i]:
        continue
    transversions += 1

print Decimal(transitions) / Decimal(transversions)
